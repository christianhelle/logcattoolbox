#include "stdafx.h"
#include "process_status.h"
#include "utils.h"
#include <time.h>
#include <fstream>
#include <sstream>

bool process_status::test()
{
	return false;
}

vector<ps_entry> process_status::parseFile(const char * filename) const
{
    fstream ifs(filename);
    string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
	return content.size() > 0 ? this->parseText(content) : vector<ps_entry>();
}

vector<ps_entry> process_status::parseText(string text) const
{
    auto start = clock();
	vector<ps_entry> entries;
    string line;

    istringstream iss(text);
    getline(iss, line);

    while (getline(iss, line))
    {
        ps_entry entry;
        entry.pid = utils::trimright(line.substr(10, 6));

        auto idx = line.find_last_of(" ");
        entry.name = line.substr(idx, line.size() - idx);
        
        entries.push_back(entry);
    }

    if (this->verboseMode)
    {
        printf("\r\nFound %ld PS entries", static_cast<long>(entries.size()));
        printf("\r\nTime taken %.2fs\n", static_cast<double>(clock() - start) / CLOCKS_PER_SEC);
    }
	return entries;
}
