#include "stdafx.h"
#include "arguments.h"

arguments::arguments()
{
    displayMode = false;
    verboseMode = false;

    help = arg_litn(nullptr, "help", 0, 1, "display this help and exit"),
    version = arg_litn(nullptr, "version", 0, 1, "display version info and exit"),
    verb = arg_litn("v", "verbose", 0, 1, "verbose output"),
    ps = arg_filen("p", nullptr, "<ps dump file>", 0, 1, "adb shell ps dump"),
    input = arg_filen("i", nullptr, "<logcat dump file>", 0, 1, "logcat dump text file"),
    output = arg_filen("o", nullptr, "<output sqlite db>", 0, 1, "output sqlite database"),
    args_end = arg_end(20);
}

bool arguments::test()
{
    char *helpArgs[] = { (char*)("--help") };
    char *versionArgs[] = { (char*)("--version") };
    char *verboseArgs[] = { (char*)("-v") };
    char *psArgs[] = { (char*)("-p"), (char*)("ps.txt") };
    char *inputArgs[] = { (char*)("-i"), (char*)("sample-logcat-threadtime-dump.txt") };
    char *outputArgs[] = { (char*)("-i"), (char*)("sample-logcat-threadtime-dump.txt") ,(char*)("-o"), (char*)("sample.db") };

    auto result = read(2, helpArgs) && read(2, versionArgs) && read(2, verboseArgs) && read(2, psArgs) && read(3, inputArgs) && read(5, outputArgs);
	if (!result)
        printf("arguments test failed");
	return result;
}

bool arguments::read(int argc, char * argv[])
{
    auto noerrors = true;
    void *argtable[] = { help, version, verb, ps, input, output, args_end };
	auto nerrors = arg_parse(argc, argv, argtable);

	if (help->count > 0 || argc == 1)
	{
		this->displayMode = true;
        printf("\nUsage: %s", APP_NAME);
		arg_print_syntax(stdout, argtable, "\n\n");
        arg_print_glossary(stdout, argtable, " %-25s %s\n");
        printf("\n");
	}

	if (version->count > 0)
	{
		this->displayMode = true;
        printf("\n%s", APP_NAME);
        printf("\nversion 1.0\n\n");
	}

    if (input->count > 0)
        this->inputFilename = *input->filename;

    if (output->count > 0)
        this->outputFilename = *output->filename;

    if (ps->count > 0)
        this->psFilename = *ps->filename;

    if (verb->count > 0)
        this->verboseMode = true;

	if (nerrors > 0)
	{
		arg_print_errors(stdout, args_end, APP_NAME);
		printf("Try '%s --help' for more information.\n", APP_NAME);
		noerrors = false;
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return noerrors;
}

const char * arguments::getInputFilename() const
{
#ifdef _WIN32
	if (IsDebuggerPresent())
		return "logcat.txt";
#endif // _WIN32
	return this->inputFilename.c_str(); 
}

const char * arguments::getOutputFilename() const
{
#ifdef _WIN32
	if (IsDebuggerPresent())
		return "logcat.db";
#endif // _WIN32
	return this->outputFilename.c_str(); 
}

const char * arguments::getPsDumpFilename() const
{
#ifdef _WIN32
	if (IsDebuggerPresent())
		return "ps.txt";
#endif // _WIN32
	return this->psFilename.c_str(); 
}
