#pragma once

#include <string>
#include <vector>

using namespace std;

struct logcat_entry {
	string date;
	string time;
	string pid;
	string tid;
	string app;
	string level;
	string tag;
	string text;
};

class logcat
{
public:
    explicit logcat(bool verboseMode) { this->verboseMode = verboseMode; }

	bool test() const;
	vector<logcat_entry> parseFile(const char *filename) const;
	vector<logcat_entry> parseText(string text) const;

private:
    bool verboseMode;
    static logcat_entry parseCsv(string currLine);
};

