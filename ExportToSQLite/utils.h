#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <fstream>

using namespace std;

class utils
{
public:
    static string replace_all(const string str, const string& from, const string& to);
    static string quotesql(const string& s);
    static string trimright(const string& s);
    static size_t filesize(const char* filename);
};

#endif // UTILS_H
