TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_MAC_SDK = macosx10.12

SOURCES += main.cpp \
    sqlite3.c \
    argtable3.c \
    arguments.cpp \
    logcat.cpp \
    stdafx.cpp \
    database.cpp \
    process_status.cpp \
    utils.cpp

HEADERS += \
    sqlite3.h \
    sqlite3ext.h \
    argtable3.h \
    arguments.h \
    logcat.h \
    stdafx.h \
    database.h \
    process_status.h \
    utils.h
