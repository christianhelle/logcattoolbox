#include "utils.h"

#include <algorithm>
#include <functional>
#include <cctype>

string utils::replace_all(std::string str, const std::string& from, const std::string& to)
{
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}

string utils::quotesql(const string& s) {
    return string("'") + replace_all(s, "\'", "\"") + string("'");
}

string utils::trimright(const string& s)
{
    auto str = s;
    str.erase(std::find_if(str.rbegin(), str.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), str.end());
    return str;
}

size_t utils::filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    size_t size = in.tellg();
    return size;
}
