#include "stdafx.h"
#include "logcat.h"
#include "arguments.h"
#include "database.h"

int main(int argc, char *argv[])
{
    auto start = clock();

	arguments args;
#ifdef _WIN32
	if (!IsDebuggerPresent())
	{
#endif // _WIN32
    if (!args.read(argc, argv))
        return 1;

    if (args.getDisplayMode())
        return 0;

    if (!args.getInputFilename())
        return 2;
#ifdef _WIN32
	}
#endif // _WIN32
		
    logcat logcat(args.getVerboseMode());
    auto logcat_entries = logcat.parseFile(args.getInputFilename());

    string output = args.getOutputFilename();
    if (output.size() == 0)
        output = string(args.getInputFilename()) + ".db";

    process_status ps(args.getVerboseMode());
    vector<ps_entry> ps_entries;
    if (args.getPsDumpFilename())
        ps_entries = ps.parseFile(args.getPsDumpFilename());

    database db(logcat_entries, ps_entries, output.c_str(), args.getVerboseMode());
    db.initialize();
	db.populate();

    printf("\nExported Logcat to SQLite database in %.2fs\n\n", static_cast<double>(clock() - start) / CLOCKS_PER_SEC);
	return 0;
}
