#pragma once

#include <string>
#include "argtable3.h"

using namespace std;

class arguments
{
public:
    arguments();
	bool test();
	bool read(int argc, char *argv[]);

	const char* getInputFilename() const;
	const char* getOutputFilename() const;
	const char* getPsDumpFilename() const;
	bool getVerboseMode() const { return this->verboseMode; }
	bool getDisplayMode() const { return this->displayMode; }

private:
	string inputFilename;
	string outputFilename;
	string psFilename;
    bool displayMode;
    bool verboseMode;
    struct arg_lit *verb, *help, *version;
	struct arg_file *ps, *input, *output;
	struct arg_end *args_end;
};
