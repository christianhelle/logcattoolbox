#include "database.h"
#include "sqlite3.h"
#include "utils.h"

database::database() : verboseMode(false)
{
}

database::database(const char* outputFilename, bool verboseMode)
{
	this->outputFilename = outputFilename;
	this->verboseMode = verboseMode;
}

database::database(const vector<logcat_entry> logcat_entries,
	const vector<ps_entry> ps_entries,
	const char * outputFilename,
	bool verboseMode)
{
	this->logcat_entries = logcat_entries;
	this->ps_entries = ps_entries;
	this->outputFilename = outputFilename;
	this->verboseMode = verboseMode;
}

bool database::initialize() const
{
    FILE *file = fopen(outputFilename.c_str(), "r");
	if (file) {
		printf("Output file already exists\n");
		fclose(file);
		return false;
	}

	string sql;
	sqlite3 *db;
	char *errMsg = nullptr;
	auto hasErrors = false;

	auto rc = sqlite3_open(this->outputFilename.c_str(), &db);
	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return false;
	}

	sql = "CREATE TABLE LOGCAT("  \
		"DATE	TEXT	NOT NULL," \
		"TIME	TEXT	NOT NULL," \
		"PID	TEXT	NOT NULL," \
		"TID	TEXT	NOT NULL," \
		"APP	TEXT    NULL," \
		"LEVEL	TEXT	NOT NULL," \
		"TAG	TEXT	NOT NULL," \
		"TEXT	TEXT	NOT NULL);";

	rc = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		printf("Unable to create LOGCAT table\n");
		fprintf(stderr, "SQL error: %s\n", errMsg);
		sqlite3_free(errMsg);
		hasErrors = true;
	}

	sqlite3_close(db);
	return !hasErrors;
}

bool database::populate() const
{
	return this->populate(this->logcat_entries, this->ps_entries);
}

bool database::populate(vector<logcat_entry> logcat_entries, vector<ps_entry> ps_entries) const
{
	auto start = clock();
	this->updateLogcatEntries(&logcat_entries, &ps_entries);

	string query;
	sqlite3 *db;
	char *errMsg = nullptr;
	auto hasErrors = false;

	auto rc = sqlite3_open(this->outputFilename.c_str(), &db);
	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return false;
	}

	query = "BEGIN;";
	for (size_t i = 0; i < logcat_entries.size(); i++)
	{
		auto entry = logcat_entries[i];
		query += "INSERT INTO LOGCAT (DATE, TIME, PID, TID, APP, LEVEL, TAG, TEXT) VALUES ("
			+ utils::quotesql(entry.date) + ","
			+ utils::quotesql(entry.time) + ","
			+ utils::quotesql(entry.pid) + ","
			+ utils::quotesql(entry.tid) + ","
			+ utils::quotesql(entry.app) + ","
			+ utils::quotesql(entry.level) + ","
			+ utils::quotesql(entry.tag) + ","
			+ utils::quotesql(entry.text) + ");\n";
	}
	query += "COMMIT;";

	rc = sqlite3_exec(db, query.c_str(), nullptr, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "Inserting to LOGCAT table failed. SQL error: %s\n", errMsg);
		sqlite3_free(errMsg);
		hasErrors = true;
	}

	sqlite3_close(db);

	if (this->verboseMode)
	{
		printf("\r\nSuccessfully populated database %ld logcat entries", +static_cast<long>(logcat_entries.size()));
		printf("\r\nTime taken %.2fs\n", static_cast<double>(clock() - start) / CLOCKS_PER_SEC);
	}

	return !hasErrors;
}

void database::updateLogcatEntries()
{
	this->updateLogcatEntries(&this->logcat_entries, &this->ps_entries);
}

void database::updateLogcatEntries(vector<logcat_entry>* logcat_entries, vector<ps_entry>* ps_entries)
{
	for (size_t i = 0; i < logcat_entries->size(); i++)
	{
		for (size_t j = 0; j < ps_entries->size(); j++)
		{
			if (logcat_entries->at(i).pid.compare(ps_entries->at(j).pid) == 0)
			{
				logcat_entries->at(i).app = ps_entries->at(j).name;
				break;
			}
		}
	}
}
