#pragma once

#include <string>
#include <vector>

using namespace std;

struct ps_entry {
	string user;
	string pid;
	string ppid;
	string vsize;
	string rss;
	string wchan;
	string pc;
	string name;
};

class process_status
{
public:
    process_status(bool verboseMode) { this->verboseMode = verboseMode; }
    static bool test();
    vector<ps_entry> parseFile(const char *filename) const;
    vector<ps_entry> parseText(string text) const;

private:
    bool verboseMode;
};

