#include "stdafx.h"
#include "logcat.h"
#include <time.h>
#include <fstream>

bool logcat::test() const
{
    auto fileResults = this->parseFile("sample-logcat-threadtime-dump.txt");
	return fileResults.size() > 0;
}

vector<logcat_entry> logcat::parseFile(const char *filename) const
{
    fstream ifs(filename);
    string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
	return content.size() > 0 ? this->parseText(content) : vector<logcat_entry>();
}

vector<logcat_entry> logcat::parseText(string text) const
{
    auto start = clock();
	vector<logcat_entry> entries;
	string currentLine;

	char c;
    auto col = 1;
    auto tagOrMsg = 0;
		
    size_t i = 0;
	while (i < text.size() && ((c = text[i++])))
	{		
		switch (col)
		{
		case  6:
		case 19:
		case 25:
		case 31:
		case 33:
			currentLine.append(",");
			break;

		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
			if (c != ' ')
				currentLine.append(1, c);
			break;

		default:
			if (col < 33)
			{
				if ((c == 0x0D) || (c == 0x0A))
				{
					col--;
				}
				else
				{
					currentLine.append(1, c);
				}
			}
			else if (tagOrMsg == 0)
			{
				if (c != ':'&&c != ' ')
				{
					currentLine.append(1, c);
				}
				else if (c == ':')
				{
					currentLine.append(",");
				    tagOrMsg = 1;
				}
			}
			else
			{
				if ((c == 0x0D) || (c == 0x0A))
				{
					if (col != 1)
					{
						col = 1;
						tagOrMsg = 0;
					}
					col--;
				}
				else if (c != '"')
				{
					currentLine.append(1, c);
				}
				else
				{
					currentLine.append("'");
					currentLine.append(1, c);
				}
			}
			break;
		}

		if (col == 1 && currentLine.size())
		{
			if (currentLine[0] != '-')
			{
			    auto entry = this->parseCsv(currentLine);
				if (entry.date.size() > 0 && entry.pid.size() > 0 && entry.level.size() > 0)
					entries.push_back(entry);
			}

#ifdef _WIN32
			OutputDebugStringA(currentLine.append("\r\n").c_str());
#endif
			currentLine.clear();
		}

		col++;
	}

    if (this->verboseMode)
    {
        printf("\r\nFound %.2ld entries", static_cast<long>(entries.size()));
        printf("\r\nTime taken %.2fs\n", static_cast<double>(clock() - start) / CLOCKS_PER_SEC);
    }
	return entries;
}

logcat_entry logcat::parseCsv(const string csv)
{
	vector<int> indices;
	indices.push_back(0);
    for (size_t i = 0; i < csv.size(); i++)
	{
		if (csv[i] == ',')
			indices.push_back(static_cast<int>(i));
		if (indices.size() == 7)
			break;
	}

	vector<string> strings;
    for (size_t i = 0; i < indices.size(); i++)
	{
		size_t count = 0;
		if (i + 1 < indices.size())
			count = indices[i + 1] - indices[i] - 1;
	    auto pos = indices[i];
		if (pos > 0)
			pos++;
		if (count == 0)
			count = csv.size() - pos;
		strings.push_back(csv.substr(pos, count));
	}
	
	if (strings.size() != 7)
		return logcat_entry();

	logcat_entry entry;
	entry.date = strings[0];
	entry.time = strings[1];
	entry.pid = strings[2];
	entry.tid = strings[3];
	entry.level = strings[4][0];
	entry.tag = strings[5];
	entry.text = strings[6];
	
	return entry;
}
