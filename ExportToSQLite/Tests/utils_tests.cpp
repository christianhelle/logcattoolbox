#include "stdafx.h"
#include "CppUnitTest.h"

#include "../utils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ExportToSQLiteTests
{		
	TEST_CLASS(utils_tests)
	{
	public:
		
		TEST_METHOD(replace_all_removes_all_dots)
		{
            std::string str = ",.,.,.";
            std::string result = utils::replace_all(str, ".", "");
            Assert::AreEqual((int)result.size(), 3);
		}

        TEST_METHOD(quotesql_surrounds_string_with_single_quotes)
        {
            std::string str = "text";
            std::string result = utils::quotesql(str);
            Assert::AreEqual(result.compare("'text'"), 0);
        }

        TEST_METHOD(trimright_removes_whitespaces_from_end)
        {
            std::string str = "text   ";
            std::string result = utils::trimright(str);
            Assert::AreEqual(result.compare("text"), 0);
        }

		TEST_METHOD(filesize_returns_valid_size)
		{
			auto filename = "test";
			ofstream stream;
			stream.open(filename);
			for (size_t i = 0; i < 10; i++)
				stream << std::to_string(rand() * (i + 1));
			stream.close();

			auto size = utils::filesize(filename);
			remove(filename);
			Assert::IsTrue(size > 0);
		}
	};
}