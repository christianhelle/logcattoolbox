#include "stdafx.h"
#include "CppUnitTest.h"

#include "../database.h"
#include "../arguments.h"
#include "../logcat.h"
#include "../utils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ExportToSQLiteTests
{
    TEST_CLASS(integration_tests)
    {
    public:

        TEST_METHOD(database_populate_integration_tests)
        {
            auto ps_entries = this->generate_ps_entries();            
            auto logcat_entries = this->generate_logcat_entries();
            auto outputFilename = std::to_string(rand()*rand()*rand()) + "1.db";
            auto file = outputFilename.c_str();

            database sut(logcat_entries, ps_entries, file, false);
            Assert::IsTrue(sut.initialize() && sut.populate());
            auto size = utils::filesize(file);
            remove(file);
            Assert::IsTrue(size > 0);
        }

        TEST_METHOD(database_populate_v2_integration_tests)
        {
            auto ps_entries = this->generate_ps_entries();
            auto logcat_entries = this->generate_logcat_entries();
            auto outputFilename = std::to_string(rand()*rand()*rand()) + "2.db";
            auto file = outputFilename.c_str();

            database sut(file, false);
            Assert::IsTrue(sut.initialize() && sut.populate(logcat_entries, ps_entries));
            auto size = utils::filesize(file);
            remove(file);
            Assert::IsTrue(size > 0);
        }

        TEST_METHOD(arguments_integration_tests)
        {            
            arguments sut;
            auto result = sut.test();
            Assert::IsTrue(result);
        }

        TEST_METHOD(logcat_integration_tests)
        {
            string str = 
                "09 - 07 15:06 : 47.334   357   360 I Ext4Crypt : ext4 crypto complete called on / data\n"\
                "09 - 07 15:06 : 47.335   357   360 I Ext4Crypt : No master key, so not ext4enc\n"\
                "09 - 07 15:06 : 47.335  2100  6111 I chatty : uid = 1000(system)expire 1 line\n"\
                "09 - 07 15:06 : 47.339  2100  6109 I chatty : uid = 1000(system)expire 68 lines\n"\
                "09 - 07 15:06 : 47.339  2100  2943 I chatty : uid = 1000(system)expire 2 lines\n"\
                "09 - 07 15:06 : 47.342  2100  6110 I chatty : uid = 1000(system)expire 4 lines\n"\
                "09 - 07 15:06 : 47.524   357  6118 D vold : Starting trim of / data\n"\
                "-------------";
            auto items = logcat(true).parseText(str);
            Assert::AreEqual(7, static_cast<int>(items.size()));
        }

    private:
        static vector<ps_entry> generate_ps_entries() 
        {
            vector<ps_entry> ps_entries;
            for (size_t i = 1; i <= 1000; i++)
            {
                ps_entry ps;
                ps.pid = "1";
                ps.name = "test";
                ps_entries.push_back(ps);
            }
            return ps_entries;
        }

        static vector<logcat_entry> generate_logcat_entries()
        {
            vector<logcat_entry> logcat_entries;
            for (size_t i = 1; i <= 1000; i++)
            {
                logcat_entry logcat;
                logcat.date = std::to_string(rand()*rand()*rand());
                logcat.time = std::to_string(rand()*rand()*rand());
                logcat.pid = std::to_string(i);
                logcat.tid = std::to_string(rand()*rand()*rand());
                logcat.app = std::to_string(rand()*rand()*rand());
                logcat.level = std::to_string(rand()*rand()*rand());
                logcat.tag = std::to_string(rand()*rand()*rand());
                logcat.text = std::to_string(rand()*rand()*rand());
                logcat_entries.push_back(logcat);
            }
            return logcat_entries;
        }
    };
}