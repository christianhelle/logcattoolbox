#pragma once

#include <vector>
#include <ctime>
#include "logcat.h"
#include "process_status.h"

class database
{
public:
    database();
    database(const char *outputFilename, bool verboseMode);
    database(const vector<logcat_entry> logcat_entries,
             const vector<ps_entry> ps_entries,
             const char *outputFilename,
             bool verboseMode);

    bool initialize() const;
	bool populate() const;
    bool populate(vector<logcat_entry> logcat_entries,
                  vector<ps_entry> ps_entries) const;

private:
    vector<logcat_entry> logcat_entries;
    vector<ps_entry> ps_entries;
	string outputFilename;
    bool verboseMode;

    void updateLogcatEntries();
    static void updateLogcatEntries(vector<logcat_entry> *logcat_entries,
                             vector<ps_entry> *ps_entries);
};
